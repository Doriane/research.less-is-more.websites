# research.less-is-more.websites

Making website by doing less and thinking more.

It can be in terms of... (of course both are linked, but some websites can focus more on some points and less on others)

* **visualy**
  * defaultism: embracing or dealing with web defaults aesthetics
  * constrainism: designs that works with limited or constrained definitions or resources
* **web tech**
  * static over dynamic
  * javascript-less
  * taking advantages of already-there techs (instead of re-creating them with external libs)
    * achors/permalinks
    * emails
    * RSS
  * size (small sized websites)
    * dealing with images
    * dealing with fonts (default `font-familly`)

